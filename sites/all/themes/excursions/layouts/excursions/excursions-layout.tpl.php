<div<?php print $attributes; ?>>
  <header class="l-header" role="banner">
    <div class="l-branding">
      <?php if ($logo): ?>
      <div class="logo">
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home" class="site-logo"><img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" /></a>
      </div>
      <?php endif; ?>

      <?php print render($page['branding']); ?>
    </div>
    <div class="menu">
      <?php print render($page['header']); ?>
      <?php print render($page['navigation']); ?>
    </div>
  </header>

  <div class="l-main">
    <?php print render($page['highlighted']); ?>
    <?php print render($page['help']); ?>
    <div class="l-content" role="main">
      
      <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h1><?php print (isset($offer_page) || isset($tour_page) ) ? $custom_title : $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <?php print render($tabs); ?>
      
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>
    </div>

    <?php print render($page['sidebar_first']); ?>
    <?php print render($page['sidebar_second']); ?>
  </div>

  <footer class="l-footer" role="contentinfo">
    <?php print render($page['footer']); ?>
    <div class="copy">
        <span class="sainet">
            <a target="_blank" href="http://www.creandopaginasweb.com">
                Página web desarrollada por <img alt="Sainet Ingenieria" src="http://www.creandopaginasweb.com/theme/img/logo_blanco.png">
            </a>
        </span>
    </div>
  </footer>
  <div id="TA_socialButtonIcon138" class="TA_socialButtonIcon"><ul id="CVmjpb" class="TA_links oUFAll"><li id="A0GTCP7jls" class="tU03HsydO"><a target="_blank" href="https://www.tripadvisor.co/Attraction_Review-g294074-d11746300-Reviews-Capital_Excursions-Bogota.html"><img src="https://www.tripadvisor.co/img/cdsi/img2/branding/socialWidget/20x28_green-21690-2.png"/></a></li></ul></div><script src="https://www.jscache.com/"></script>
</div>
