<?php

/**
 * Implements hook_preprocess_page().
 */
function excursions_preprocess_page(&$variables) {
  // You can use preprocess hooks to modify the variables before they are passed
  // to the theme function or template file.
	if (isset($variables['node']) && ('offer' == $variables['node']->type)) {
		$variables['offer_page'] = TRUE;
		$variables['custom_title'] = t('Ofertas');
	}
	if (isset($variables['node']) && ('tour' == $variables['node']->type)) {
		$variables['tour_page'] = TRUE;
		$variables['custom_title'] = t('Tours');
	}
}
